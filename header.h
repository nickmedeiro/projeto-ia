#include <stdio.h>
#include <iostream>
#include <utility>
#include <time.h>
#include <bits/stdc++.h>
#define IMPACT 3

using namespace std;

struct node {
    pair<float,float> piece;
    struct node *left, *right;
};

void startGame(char level); 
struct node *newNode(pair<float,float> piece);
struct node *insertRight(struct node *node, pair<float,float> piece);
struct node *insertLeft(struct node *node, pair<float,float> piece);
struct node *insertInBoard(struct node *root, pair<float,float> piece); //  remove da m�o do jogador, e coloca a pe�a no tabuleiro
void showBoard(struct node *root);
pair<float,float> flipPiece(pair<float,float> piece); //Vira a pe�a se necess�rio, para encaixar no tabuleiro
float endRound(int winner, vector< pair<float,float> > playerHand, vector< pair<float,float> > machineHand);
float calculate(int winner, vector< pair<float,float> > playerHand, vector< pair<float,float> > machineHand);
vector< pair<float,float> >  createPieces();
vector< pair<float,float> > generateHand(vector< pair<float,float> > allPieces, vector< pair<float,float> > Hand);
vector< pair<float,float> > shuffle(vector< pair<float,float> > allPieces);       
void showHand(vector< pair<float,float> > playerHand); 
bool searchDouble(vector< pair<float,float> > playerHand,vector< pair<float,float> > machineHand);
int boardSize(struct node *root, int count);
struct node *playerTurn(struct node *root, vector< pair<float,float> >& playerHand, vector< pair<float,float> >& stack, int machineSize);

struct node *machineTurn(vector< pair<float,float> >& machineHand, vector< pair<float,float> >& stack, struct node *root, int playerSize, char level);
bool checkPlay(struct node *root, pair<float,float> piece);
float probability(vector< pair<float,float> > machineHand, struct node *root, int stackSize, int playerSize, float left, float right);
int inBoard(float key, int value, struct node *root);
int inHand(float key, vector< pair<float,float> > machineHand);
struct node *machineInsertInBoard(struct node *root, pair<float,float> piece, char side);
int defensive (pair<float,float> edges , vector< pair<float,float> > machineHand, int index);
int greedy(vector< pair<float,float> > machineHand, vector<int> playable, pair<float,float> edges);
int aStar(vector< pair<float,float> > machineHand, vector<int> playable, pair<float,float> edges,struct node *root, int stackSize, int playerSize);

//void machineTest(vector< pair<float,float> > machineHand, vector< pair<float,float> > stack, struct node *root, int playerSize);
