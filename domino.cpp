#include "header.h"

int main(){
    char choice, level; //para o menu inicial

    do
    {
        cout << "\t|| ======= Domino Monetario ======= ||";
        cout << "\n\t\t Pronto para iniciar o jogo ?"; 
        cout << "\n\t\t S - Sim\n\t\t N - Nao\n\t Opcao [ ]\b\b";
        cin >> choice;
        if(choice != 's' && choice != 'S' && choice != 'n'&& choice != 'N') {
            cout << "\n\nOpcao invalida !!\n\n";
            system("pause");
            system("cls");
        }
        if(choice == 's' || choice == 'S'){
            do{
            cout << "\n\t\t Escolha a difculdade ?"; 
            cout << "\n\t\t F - Facil (Guloso)\n\t\t D - Dificil (A *)\n\t Opcao [ ]\b\b";
            cin >> level;
            }while(level != 'f' && level != 'F' && level != 'd'&& level != 'D');
            startGame(level);
        }
    }while(choice != 's' && choice != 'S' && choice != 'n'&& choice != 'N');

    return 0;
}

//Inicia o jogo após o menu inicial na main()
void startGame(char level){
    bool turn;
    int round = 1, winner, control=0;//numero da atual partida (total 3) mudar o winner pra bool ?
    float playerSavings = 0, machineSavings = 0;
    vector< pair<float,float> > stack;
    vector< pair<float,float> > playerHand;
    vector< pair<float,float> > machineHand;
    struct node *root = NULL;


    do{
        system("cls");
        root = NULL;
        
        stack.erase(stack.begin(),stack.end());
        stack = createPieces();      

        playerHand.erase(playerHand.begin(), playerHand.end());
        playerHand = generateHand(stack, playerHand);
        stack.erase(stack.end()-12,stack.end());

        machineHand.erase(machineHand.begin(), machineHand.end());
        machineHand = generateHand(stack, machineHand);
        stack.erase(stack.end()-12,stack.end());
        
        turn = searchDouble(playerHand, machineHand); //define quem começa procurando pelas peças duplas de maior valor
    
        do{
            control = machineHand.size() + playerHand.size();
            if(turn){
                cout << "\nPlayer Turn";
                root = playerTurn(root, playerHand, stack, machineHand.size());
            }else{
                cout << "\nMachine Turn";
                root = machineTurn(machineHand, stack, root, playerHand.size(), level); //essa função tem que ter as 2 opções guloso e A*
            }

            if(playerHand.size() == 0 || machineHand.size() == 0){
                if(playerHand.size() < machineHand.size())
                {
                    playerSavings += endRound(1, machineHand, playerHand);
                    system("pause");
                }    
                else
                {
                    machineSavings += endRound(2, machineHand, playerHand);
                    system("pause");
                }
                    
                round++;
            }

            if(stack.size() == 0 && control == ( machineHand.size() + playerHand.size())){
                float machine, player;
                for(int i=0; i<machineHand.size(); i++)
                    machine += machineHand[i].first + machineHand[i].second;
                for(int i=0; i<playerHand.size(); i++)
                    player += playerHand[i].first + playerHand[i].second;
                if(machine > player)
                    playerSavings = machine;
                if(machine < player)
                    machineSavings = player;
                if(machine == player)
                    cout << "\n A rodada empatou!";
                    system("pause");
                round++;
            }

            turn = !turn;  
        }while(playerHand.size() > 0 && machineHand.size() > 0);
        
    }while(round < 4);

    if(playerSavings > machineSavings) 
        cout << "\n\tVoce venceu o jogo!";

    if(playerSavings < machineSavings)
        cout << "\n\tA maquina venceu :(";

    if(playerSavings == machineSavings)
        cout << "\n\t O jogo empatou !\n";
    
    cout << "\nPoupanca jogador R$:" << playerSavings << "\n";
    cout << "\nPoupanca maquina R$:" << machineSavings << "\n";

    system("pause");
}

// Cria todas as peças do jogo, e retorna a pilha de compras
vector< pair<float,float> > createPieces(){
    vector< pair<float,float> > allPieces;
    float values[] = {0, 0.05, 0.1, 0.25, 0.5, 1, 2, 5, 10, 20, 50, 100, 200};

    for(int i=0; i<13; i++){    
        for(int j=i; j<13; j++){
            pair<float,float> newPiece;
            newPiece.first = values[i];
            newPiece.second = values[j];
            allPieces.push_back(newPiece);
        }
    }

    allPieces = shuffle(allPieces);  

    return allPieces;
}

// Embaralha as peças
vector< pair<float,float> > shuffle(vector< pair<float,float> > stack){
    int random;
    pair< float,float> piece;
    srand(time(0));
    for(int i=0; i<130; i++){
        random = rand() % stack.size();
        piece.first = stack[random].first;
        piece.second = stack[random].second;
        if(random%2 == 1){
            stack.erase(stack.begin()+random);
            stack.push_back(piece);
        }else{
            stack.erase(stack.begin()+random);
            stack.insert(stack.begin(), piece);
        }
        
    }

    return stack;
}

// Distribui peças para o jogador e para a máquina
vector< pair<float,float> > generateHand(vector< pair<float,float> > allPieces, vector< pair<float,float> > hand){
    pair<float,float> aux;
    for(int i=1; i<13; i++){ 
        aux.first = allPieces[allPieces.size()-i].first;
        aux.second = allPieces[allPieces.size()-i].second;
        //cout << "\n" << i << " [" << aux.first << "|" << aux.second << "] \n";
        hand.push_back(aux);
        //cout << " [" << hand[i-1].first << "|" << hand[i-1].second << "] \n";
    }

    return hand;
}

bool searchDouble(vector< pair<float,float> > playerHand,vector< pair<float,float> > machineHand){
    int playerHigh = 0, machineHigh = 0;
    for(int i=0; i<machineHand.size() ; i++)
        if(machineHand[i].first == machineHand[i].second && machineHand[i].first > machineHigh)
            machineHigh = machineHand[i].first;
    
    for(int i=0; i<playerHand.size() ; i++)
        if(playerHand[i].first == playerHand[i].second && playerHand[i].first > playerHigh)
            playerHigh = playerHand[i].first;

    if(playerHigh == 0 && machineHigh == 0){
         for(int i=0; i<machineHand.size() ; i++)
            if(machineHand[i].first + machineHand[i].second > machineHigh)
                machineHigh = machineHand[i].first + machineHand[i].second;
    
        for(int i=0; i<playerHand.size() ; i++)
            if(playerHand[i].first + playerHand[i].second > playerHigh)
                playerHigh = playerHand[i].first + playerHand[i].second;
    }

    if(playerHigh > machineHigh)
        return true;
    
    return false;
}

//Mostra na tela as peças da mão do jogador
void showHand(vector< pair<float,float> > hand){    
    for(int i=0; i<hand.size(); i++)
        cout << "\t" << i << " [" << hand[i].first << "|" << hand[i].second << "] \n"; 
}

// inverte o lado da peça para inserir no tabuleiro
pair<float,float> flipPiece(pair<float,float> piece){
    float aux;
    aux = piece.first;
    piece.first = piece.second;
    piece.second = aux;

    return piece;
}

// Finaliza a rodada
float endRound(int winner, vector< pair<float,float> > playerHand, vector< pair<float,float> > machineHand){
    if(winner == 1){
        cout << "\n\t Parabens voce venceu !\n";
        return calculate(1, machineHand, playerHand);
    }else{
        cout << "\n\t Que pena a maquina venceu \n";
        return calculate(2, machineHand, playerHand);
    }
}

// Calcula o lucro da rodada
float calculate(int winner, vector< pair<float,float> > playerHand, vector< pair<float,float> > machineHand){
    float profit = 0;
    if(winner == 1){//jogador venceu
        for(int i=0; i<machineHand.size(); i++)
            profit += machineHand[i].first + machineHand[i].second;
    }
    if(winner == 2){//Maquina venceu
        for(int i=0; i<playerHand.size(); i++)
            profit += playerHand[i].first + playerHand[i].second;
    }

    return profit;
}

// Insere a peça escolhida no tabuleiro
struct node *insertInBoard(struct node *root, pair<float,float> piece){
    if(root == NULL)
        return newNode(piece);

    struct node *leftSide = root;
    struct node *rightSide = root;
         
    while(rightSide->right != NULL)
        rightSide = rightSide->right;

    while(leftSide->left != NULL)
        leftSide = leftSide->left;

    if((rightSide->piece.second == piece.first || rightSide->piece.second == piece.second)
        &&(leftSide->piece.first == piece.first || leftSide->piece.first == piece.second)){
        char choice; //para o menu 
        do
        {
            cout << "\n\t A peca pode ser jogada em ambos os lados!";
            cout << "\n\t Qual lado deseja jogar ?"; 
            cout << "\n\t\t R - Direita\n\t\t L - esquerda\n\t Opcao [ ]\b\b";
            cin >> choice;
            if(choice != 'r' && choice != 'R' && choice != 'l'&& choice != 'L') {
                cout << "\n\nOpcao invalida !!\n\n";
                system("pause");
                system("cls");
            }
            if(choice == 'r' || choice == 'R'){
                cout << "\n\t O lado Direito foi selecionado!";
                if(rightSide->piece.second == piece.second)
                    piece = flipPiece(piece); 
                rightSide->right = insertRight(rightSide, piece);
            }
            if(choice == 'l' || choice == 'L'){
                cout << "\n\t O lado Esquerdo foi selecionado!";
                if(leftSide->piece.first == piece.first)
                    piece = flipPiece(piece);
                leftSide->left = insertLeft(leftSide, piece);
            }
        }while(choice != 'r' && choice != 'R' && choice != 'l'&& choice != 'L');

        return root;
    }

    if(rightSide->piece.second == piece.first)
        rightSide->right = insertRight(rightSide, piece);
    else if(rightSide->piece.second == piece.second){
        piece = flipPiece(piece);
        rightSide->right = insertRight(rightSide, piece);
    }else if(leftSide->piece.first == piece.first){
        piece = flipPiece(piece);
        leftSide->left = insertLeft(leftSide, piece);
    }else if(leftSide->piece.first == piece.second){
        leftSide->left = insertLeft(leftSide, piece);
    }

    return root;
}

//Cria um novo no
struct node *newNode(pair<float,float> piece) {
    struct node *temp = (struct node *)malloc(sizeof(struct node));
    temp->piece.first = piece.first;
    temp->piece.second = piece.second;
    temp->left = temp->right = NULL;
    return temp;
}

// Insere a direita na arvore do tabuleiro
struct node *insertRight(struct node *node, pair<float,float> piece) {
  // Return a new node if the tree is empty
    if (node == NULL) 
        return newNode(piece);

    if(node->piece.second == piece.first)
        node->right = insertRight(node->right, piece);

    
    return node->right;
}

// Insere a esquerda na arvore do tabuleiro
struct node *insertLeft(struct node *node, pair<float,float> piece) {
  // Return a new node if the tree is empty
    if (node == NULL) 
        return newNode(piece);

    if(node->piece.first == piece.second)
        node->left = insertLeft(node->left, piece);

    return node->left;
}

// Mostra o atual tabuleiro
void showBoard(struct node *root) {
  if (root != NULL) {
    showBoard(root->left);
    cout << " [" << root->piece.first << "|" << root->piece.second << "] ";
    showBoard(root->right);
  }
}

pair<float,float> searchEdges(struct node *root, pair<float,float> edges){
    if (root != NULL) {
        struct node *leftSide = root;
        struct node *rightSide = root;

        while(leftSide->left != NULL)
            leftSide = leftSide->left;
        edges.first = leftSide->piece.first;

        while(rightSide->right != NULL)
            rightSide = rightSide->right;
        edges.second = rightSide->piece.second;

    }else{
        edges.first = 500;
        edges.second = 500;
    }



    return edges;
}

// Verifica se a peca foi jogada
bool checkPlay(struct node *root, pair<float,float> piece) {
  if (root != NULL) {
    struct node *leftSide = root;
    struct node *rightSide = root;

    while(leftSide->left != NULL)
        leftSide = leftSide->left;
    while(rightSide->right != NULL)
        rightSide = rightSide->right;

    if((leftSide->piece.first == piece.first && leftSide->piece.second == piece.second) 
    ||(leftSide->piece.first == piece.second && leftSide->piece.second == piece.first))
        return true;
    if((rightSide->piece.first == piece.first && rightSide->piece.second == piece.second) 
    ||(rightSide->piece.first == piece.second && rightSide->piece.second == piece.first))
        return true;
  }

  return false;
}

struct node *playerTurn(struct node *root, vector< pair<float,float> >& playerHand, vector< pair<float,float> >& stack, int machineSize){
   float choice;
   int count = 0, aux = 0;
   pair<float,float> piece;
   struct node *begin = root;

    do{
        cout << "\n\t   Tabuleiro:\n";
        showBoard(root); //mostra na tela o estado atual do tabuleiro
        cout << "\nPecas na mao da maquina: " << machineSize << "\n";
    
        showHand(playerHand);

        cout << "\n\t Escolha a peca que deseja jogar"; 
        cout << "\n\t Ou -1 para comprar uma nova peca:";
        if(root == NULL)
            cout << "\n\t Voce deve iniciar com uma peca dupla!";
        cin >> choice;
        if(choice < -1 || choice > playerHand.size()) {
            cout << "\n\n Opcao invalida !!\n\n";
            system("pause");
            //system("cls");
        }
        if(choice == -1){
            if(stack.size() >= 1){
                cout << "\nPecas restantes no monte:" << stack.size();
                cout << "\n\t Comprando...\n";
                piece.first = stack.back().first;
                piece.second = stack.back().second;
                playerHand.push_back(piece);
                stack.pop_back();
                cout << "\n" << stack.size();
            }else{
                cout << "\n Nao ha mais pecas a serem compradas!";
                return root;
            }
            
        }
        if(choice > -1 && choice < playerHand.size()){
            cout << "\n\t Selecionado: " << "[" << playerHand[choice].first << "|" << playerHand[choice].second << "]";
            root = insertInBoard(root, playerHand[choice]);

            if(checkPlay(root, playerHand[choice])) 
                playerHand.erase(playerHand.begin()+choice);
            else{
                cout << "\n\t Jogada invalida!\n";
                choice = -10;
            }
        }
    }while(choice <= -1 || choice > playerHand.size());
    
    return root;
}

// Conta quantas vezes a chave aparece na mao
int inHand(float key, vector< pair<float,float> > machineHand){
    int count = 0;
    for(int i = 0; i < machineHand.size(); i++){
        if(machineHand[i].first == key)
            count ++;
        else if(machineHand[i].second == key)
            count ++;
    }

    return count;
}

// Conta quantas vezes a chave aparece no tabuleiro
int inBoard(float key, int value, struct node *root){
    if (root == NULL) 
        return value;

    inBoard(key, value, root->left);
    if(root->piece.first == key)  
        value++;
    if(root->piece.second == key)
        value++;
    inBoard(key, value, root->right);
  
    return value;
}

// Calcula a probabilidade do adversario ter uma peca compativel
float probability(vector< pair<float,float> > machineHand, struct node *root, int stackSize, int playerSize, float left, float right){
    float probLeft = float(13 - inHand(left, machineHand) - inBoard(left, 0, root)) / float(playerSize + stackSize);

    if(left == right)
        return 1-probLeft;

    float probRight = float(13 - inHand(right, machineHand) - inBoard(right, 0, root)) / float(playerSize + stackSize);
   
    probLeft = probLeft * probRight;

    return 1-probLeft;
}

vector<int> searchInHand(pair<float,float> edges, vector< pair<float,float> > machineHand){
    vector<int> playable;
    for(int i = 0; i < machineHand.size(); i++)
        if(machineHand[i].first==edges.first || machineHand[i].first==edges.second 
        || machineHand[i].second==edges.first || machineHand[i].second==edges.second )
            playable.push_back(i);
    
    return playable;
}

struct node *machineInsertInBoard(struct node *root, pair<float,float> piece, char side){
    if(root == NULL)
        return newNode(piece);

    if(side == 'L'){
        struct node *leftSide = root;  
        while(leftSide->left != NULL)
            leftSide = leftSide->left;

        if(leftSide->piece.first == piece.first)
            piece = flipPiece(piece);
        leftSide->left = insertLeft(leftSide, piece);
    }else{
        struct node *rightSide = root;       
        while(rightSide->right != NULL)
            rightSide = rightSide->right;

        if(rightSide->piece.second == piece.second)
            piece = flipPiece(piece); 
        rightSide->right = insertRight(rightSide, piece);
    }

    return root;
}
   
struct node *machineTurn(vector< pair<float,float> >& machineHand, vector< pair<float,float> >& stack, struct node *root, int playerSize, char level){
    pair<float,float> edges = searchEdges(root, edges); //Encontra quais valores podem ser jogados, 
    //first -> esquerda, second -> direita
    vector<int> playable = searchInHand(edges, machineHand); //Lista as pecas que podem ser jogadas em um vector
    pair<float,float> piece;
    int choice = 0, aux = 0; //peca que sera jogada, deve existir em playable
    char side = 'L';

    //showHand(machineHand);

    //for(int i=0; i<playable.size(); i++)
        //cout << "\t" << playable[i];

    if(root == NULL){
        cout << "\n\t Maquina inicia o jogo ...";
        for(int i = 0; i < machineHand.size(); i++)
            if(machineHand[i].first == machineHand[i].second && machineHand[i].first > aux)
                choice = i;

        root = machineInsertInBoard(root, machineHand[choice], side);
        return root;
    }

    while(playable.size() == 0){
        if(stack.size() >= 1){
            cout << "\n\t Comprando cartas...";
            for(int i=0; i<playable.size(); i++)
                cout << "\t" << playable[i];

            piece.first = stack.back().first;
            piece.second = stack.back().second;
            machineHand.push_back(piece);
            stack.pop_back();
            playable = searchInHand(edges, machineHand);
        }else{
            cout << "\n\t Nao existem pecas na pilha!";
            return root;
        }
        
    }

    cout << "\n\t Selecionando...";

    if(level == 'f' || level == 'F')
        choice = greedy(machineHand, playable, edges);
    else if(level == 'd' || level == 'D')
        choice = aStar(machineHand, playable, edges, root, stack.size(), playerSize);

    cout << "\n\t Selecionado: " << choice << "[" << machineHand[choice].first << "|" << machineHand[choice].second << "]";
    //jogada, insere no tabuleiro
    if(machineHand[choice].first == edges.second || machineHand[choice].second == edges.second)
        side = 'R';
    root = machineInsertInBoard(root, machineHand[choice], side);

    if(checkPlay(root, machineHand[choice])){
        cout << "\n\t Jogada Realizada";    
        machineHand.erase(machineHand.begin()+choice);
    } 
        
    return root;
}

int defensive(pair<float,float> edges , vector< pair<float,float> > machineHand, int index)
{
    bool left = false, right = false;
    int options = 0, max = 0;
    
    if(edges.first == machineHand[index].first){
        edges.first = machineHand[index].second;
        machineHand.erase(machineHand.begin()+index);
        for(int i = 0; i < machineHand.size(); i++){
            if(machineHand[i].first == edges.first || machineHand[i].second == edges.first)
                left == true;
            if(machineHand[i].first == edges.second || machineHand[i].second == edges.second)
                right == true;
        }

        if(left) options++;
        if(right) options++;

        if(max < options)
            max = options;   
    }
    if(edges.first == machineHand[index].second){
        edges.first = machineHand[index].first;
        machineHand.erase(machineHand.begin()+index);
        for(int i = 0; i < machineHand.size(); i++){
            if(machineHand[i].first == edges.first || machineHand[i].second == edges.first)
                left == true;
            if(machineHand[i].first == edges.second || machineHand[i].second == edges.second)
                right == true;
        }

        if(left) options++;
        if(right) options++;

        if(max < options)
            max = options;         
    }
    if(edges.second == machineHand[index].second){
        edges.second = machineHand[index].first;
        machineHand.erase(machineHand.begin()+index);
        for(int i = 0; i < machineHand.size(); i++){
            if(machineHand[i].first == edges.first || machineHand[i].second == edges.first)
                left == true;
            if(machineHand[i].first == edges.second || machineHand[i].second == edges.second)
                right == true;
        }

        if(left) options++;
        if(right) options++;

        if(max < options)
            max = options;   
    }
    if(edges.second == machineHand[index].first){
        edges.first = machineHand[index].second;
        machineHand.erase(machineHand.begin()+index);
        for(int i = 0; i < machineHand.size(); i++){
            if(machineHand[i].first == edges.first || machineHand[i].second == edges.first)
                left == true;
            if(machineHand[i].first == edges.second || machineHand[i].second == edges.second)
                right == true;
        }

        if(left) options++;
        if(right) options++;

        if(max < options)
            max = options;   
    }

    return max;
}

int greedy(vector< pair<float,float> > machineHand, vector<int> playable, pair<float,float> edges){

    int options = 0, max = 0, choice = -1;

    if(playable.size() == 1)
        return playable[0];
    
    for(int i = 0; i<playable.size(); i++){
        options = defensive(edges, machineHand, playable[i]);
        if(options > max){
            max = options;
            choice = playable[i];
        }else if(options == max){
            if((machineHand[playable[i]].first + machineHand[playable[i]].second) 
            > (machineHand[choice].first + machineHand[choice].second))
                choice = playable[i];
        }      
    }

    return choice;
}

int aStar(vector< pair<float,float> > machineHand, vector<int> playable, pair<float,float> edges,struct node *root, int stackSize, int playerSize){
    float options = 0, max = 0; 
    int choice = -1;

    if(playable.size() == 1)
        return playable[0];

    for(int i = 0; i<playable.size(); i++){
        if(machineHand[playable[i]].first == edges.first){
            options = defensive(edges, machineHand, playable[i]) + (probability(machineHand, root, stackSize, playerSize, machineHand[playable[i]].second, edges.second) * IMPACT);
            if(options > max){
                max = options;
                choice = playable[i];
            }
        }
        if(machineHand[playable[i]].second == edges.first){
            options = defensive(edges, machineHand, playable[i]) + (probability(machineHand, root, stackSize, playerSize, machineHand[playable[i]].first, edges.second) * IMPACT);
            if(options > max){
                max = options;
                choice = playable[i];
            }
        }
        if(machineHand[playable[i]].first == edges.second){
            options = defensive(edges, machineHand, playable[i]) + (probability(machineHand, root, stackSize, playerSize, machineHand[playable[i]].second, edges.first) * IMPACT);
            if(options > max){
                max = options;
                choice = playable[i];
            }
        }
        if(machineHand[playable[i]].second == edges.second){
            options = defensive(edges, machineHand, playable[i]) + (probability(machineHand, root, stackSize, playerSize, machineHand[playable[i]].first, edges.first) * IMPACT);
            if(options > max){
                max = options;
                choice = playable[i];
            }
        }
                 
    }

    return choice;
}

